# CAD Files #

Please see below a generic description of the files in this directory:


## Acrylic Parts ##

|File name | Description|
|-|-|
|FF_GF_CHASSIS_V1_1_Transparent_Sliding_Lid_Acryl_3mm | 3mm acrylic part (sliding lid)|
|FF_GF_CHASSIS_V1_1_ACRYL_Parts_3mm | 3mm acrylic parts|
|FF_GF_CHASSIS_V1_1_ACRYL_Parts_5mm | 5mm acrylic parts|
|FF_GF_DISK_V1_1_40_Slots_AP_Transparent_Acryl_4mm | 4mm acrylic part (slot disk)|


## 3D Printed Parts ##

|File name | Description|
|-|-|
|FF_GF_Motor_Interface_Block_V1_1 | Motor interface part|

## Bill of Materials ##

|File name | Description|
|-|-|
|Fish_Feeder-BOM | Fish feeder list of materials|
 
## 3D Model ##

|File name | Description|
|-|-|
|A3_FF_GF_v1_1 | 2D drawings |
|FF_GFSP_V1_1 | 3D CAD Model|


To visualize the 3D model of the feeder (and correspondent exploded view) you can download the eDrawings viewer here: https://www.edrawingsviewer.com/download-edrawings and open the 3D model above.



